package com.example.simplifydriver.HelperClasses;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.example.simplifydriver.Activity.DashboardActivity;
import com.example.simplifydriver.Models.LoginResponse;
import com.example.simplifydriver.Networking.ApiService;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyService extends Service {
    private static final String TAG = "MyService";
    FusedLocationProviderClient fusedClient;
    private LocationRequest mRequest;
    private LocationCallback mCallback;
    String userLat = null, userLong = null, inserted_Date = "";
    Location lastLocation = null;
    int pauseTime = 0;
    Integer interval;
    private static MyService instance = null;

    public static boolean isInstanceCreated() {
        return instance != null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        final String userId = SharedPrefManager.getInstance(getApplicationContext()).getUserId();
        final String orderId = new SharedPreference2().getStartedOrder(getApplicationContext()).getOrderId().toString();
        final String token = SharedPrefManager.getInstance(getApplicationContext()).getUserToken();
        final String totalDistance = SharedPrefManager.getInstance(getApplicationContext()).getKeyTotalDistance();
        interval = SharedPrefManager.getInstance(getApplicationContext()).getKeyUserLocationInterval();

        fusedClient = LocationServices.getFusedLocationProviderClient(getApplicationContext());
        mCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    Log.d("TAG", "locationResult null");
                    return;
                }
                Log.d("TAG", "received " + locationResult.getLocations().size() + " locations");
                for (Location loc : locationResult.getLocations()) {
                    Log.e(TAG, "Location- "+loc.getLatitude() + " " + loc.getLongitude());
                    userLat = String.valueOf(loc.getLatitude());
                    userLong = String.valueOf(loc.getLongitude());
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                    inserted_Date = sdf.format(new Date());
                    String location = getAddress(loc.getLatitude(), loc.getLongitude());
                    Toast.makeText(MyService.this, "Your location: " + location, Toast.LENGTH_SHORT).show();
                    String state = "running";
                    if (lastLocation != null) {
                        float[] results = new float[1];
                        Location.distanceBetween(loc.getLatitude(), loc.getLongitude(),
                                lastLocation.getLatitude(), lastLocation.getLongitude(), results);
                        Log.e("LocationDistance", results[0] + "");
                        if (results[0] < 20) {
                            pauseTime = interval + pauseTime;
                        } else {
                            callApi(userId, orderId, userLat, userLong, location, state, results[0], pauseTime, totalDistance, token);
                        }
                    } else {
                        callApi(userId, orderId, userLat, userLong, location, state, 0, pauseTime, totalDistance, token);
                    }
                    lastLocation = loc;

                }
            }

            @Override
            public void onLocationAvailability(LocationAvailability locationAvailability) {
                Log.d("TAG", "locationAvailability is " + locationAvailability.isLocationAvailable());
                super.onLocationAvailability(locationAvailability);
            }

        };
        locationWizardry();
        startLocationUpdates();

    }

    public String getAddress(double lat, double lng) {
        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
        String location = "Not found";
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            Address obj = addresses.get(0);
            String add = obj.getAddressLine(0);

            Log.v("getAddress", "Address" + add);
            location = add;
        } catch (IOException e) {
            e.printStackTrace();

        }
        return location;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        instance = null;
        if (fusedClient != null) {
            fusedClient.removeLocationUpdates(mCallback);
        }
    }

    private void callApi(String userId, String orderId, String lat, String lng, String curLocation, String vehicle_state, float totalRun, int pausetime, String totalDist, String token) {
        if (new ApplicationClass().isInternetAvailable(this)) {
            Log.e(TAG, "Body- " + userId + ", " + orderId + ", " + lat + ", " + lng);
            Toast.makeText(MyService.this, "Calling api...", Toast.LENGTH_SHORT).show();
            RequestBody user_id = RequestBody.create(MultipartBody.FORM, userId);
            RequestBody order_id = RequestBody.create(MultipartBody.FORM, orderId);
            RequestBody user_lat = RequestBody.create(MultipartBody.FORM, lat);
            RequestBody user_lng = RequestBody.create(MultipartBody.FORM, lng);
            RequestBody location = RequestBody.create(MultipartBody.FORM, curLocation);
            RequestBody state = RequestBody.create(MultipartBody.FORM, vehicle_state);
            RequestBody pause_time = RequestBody.create(MultipartBody.FORM, String.valueOf(pausetime));
            RequestBody total_run = RequestBody.create(MultipartBody.FORM, String.valueOf(totalRun));
            RequestBody total_distance = RequestBody.create(MultipartBody.FORM, totalDist);

            Call<LoginResponse> call = ApiService.createApiService().saveLiveLocation(user_id, order_id,
                    user_lat, user_lng, location, state, total_run, pause_time, total_distance, "Bearer " + token);
            call.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    try {
                        Log.e(TAG, response.body().getMessage());
                        pauseTime = 0;
                        Toast.makeText(MyService.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    Log.e(TAG, t.getMessage());
                    Toast.makeText(MyService.this, "onFailure Api:" + t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(this, "No internet connection.", Toast.LENGTH_SHORT).show();
        }

    }

    @SuppressLint("MissingPermission")
    protected void startLocationUpdates() {
        fusedClient.requestLocationUpdates(mRequest, mCallback, null);
    }


    @SuppressLint("MissingPermission")
    private void locationWizardry() {
        //Initially, get last known location. We can refine this estimate later
        fusedClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    String loc = location.getProvider() + ":Accu:(" + location.getAccuracy() + "). Lat:" + location.getLatitude() + ",Lon:" + location.getLongitude();
                    Log.e("locaitonWizardry", loc);
                    //getAddress(location.getLatitude(), location.getLongitude());
                }
            }
        });

        //now for receiving constant location updates:
        mRequest = new LocationRequest();
        mRequest.setInterval(interval);
        mRequest.setFastestInterval(interval);
        mRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mRequest);

        //This checks whether the GPS mode (high accuracy,battery saving, device only) is set appropriately for "mRequest". If the current settings cannot fulfil
        //mRequest(the Google Fused Location Provider determines these automatically), then we listen for failutes and show a dialog box for the user to easily
        //change these settings.
        SettingsClient client = LocationServices.getSettingsClient(getApplicationContext());
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());
        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    // Location settings are not satisfied, but this can be fixed
                    // by showing the user a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(ApplicationClass.getCurrentActivity(), 500);
                    } catch (IntentSender.SendIntentException sendEx) {
                        // Ignore the error.
                    }
                }
            }
        });

        //actually start listening for updates: See on Resume(). It's done there so that conveniently we can stop listening in onPause
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String NOTIFICATION_CHANNEL_ID = "TapLogChannel";
        String channelName = "Location Service Channel";
        NotificationChannel chan = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_NONE);
        chan.setLightColor(Color.BLUE);
        chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);

        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert manager != null;
        manager.createNotificationChannel(chan);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        Notification notification = notificationBuilder.setOngoing(true)
                .setContentTitle("Simplify is running in background for location updates")
                .setPriority(NotificationManager.IMPORTANCE_HIGH)
                .setCategory(Notification.CATEGORY_SERVICE)
                .build();
        startForeground(2, notification);
        Log.e(TAG, "onStartCommand");

        return START_STICKY;
    }


}
