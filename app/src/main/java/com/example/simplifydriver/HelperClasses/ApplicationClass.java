package com.example.simplifydriver.HelperClasses;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.os.Handler;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import androidx.appcompat.app.AlertDialog;

import com.example.simplifydriver.Activity.SplashActivity;
import com.example.simplifydriver.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Objects;

public class ApplicationClass extends Application {
    private static AlertDialog dialog;
    private static Activity mCurrentActivity ;

    public void restartApp(Context context){
        Intent intent = new Intent(context.getApplicationContext(), SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static Activity getCurrentActivity () {
        return mCurrentActivity ;
    }
    public void setCurrentActivity(Activity mCurrentActivity) {
        ApplicationClass.mCurrentActivity = mCurrentActivity ;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
    }

    public boolean isInternetAvailable(Context context){
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getActiveNetwork() != null) {
            NetworkCapabilities nc = cm.getNetworkCapabilities(cm.getActiveNetwork());
            return nc.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) || nc.hasTransport(
                    NetworkCapabilities.TRANSPORT_WIFI
            );
        }
        return false;
    }

    public static void changeBgToRed(View ...view){
        for (View v: view){
            v.setBackgroundResource(R.drawable.rounder_border_input_red);
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                for (View v: view){
                    v.setBackgroundResource(R.drawable.rounder_border_input);
                }
            }
        }, 1000);
    }

    public static void showAlertMsg(Context context, String msg) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    public static void showAlertMsgWithFinish(Activity context, String msg) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                        context.finish();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    public static String convertTimeFormat(String serverTime){
        DateFormat inputFormat = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
        DateFormat outputFormat = new SimpleDateFormat("K:mm a", Locale.getDefault());
        String time = "";
        try {
            time = outputFormat.format(Objects.requireNonNull(inputFormat.parse(serverTime)));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return time;
    }

    public static String convertTimeFormat(String oldFormat, String newFormat, String value){
        DateFormat inputFormat = new SimpleDateFormat(oldFormat, Locale.getDefault());
        DateFormat outputFormat = new SimpleDateFormat(newFormat, Locale.getDefault());
        String time = "";
        try {
            time = outputFormat.format(Objects.requireNonNull(inputFormat.parse(value)));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return time;
    }


    public static String convertTimeFormat2(String choosedTime){
        DateFormat inputFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
        DateFormat outputFormat = new SimpleDateFormat("K:mm a", Locale.getDefault());
        String time = "";
        try {
            time = outputFormat.format(Objects.requireNonNull(inputFormat.parse(choosedTime)));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return time;
    }

    public static void showProgressDialog(Activity activity){
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        hideKeyboard(activity);
        View dialogView      = LayoutInflater.from(activity).inflate(R.layout.loader, null);
        builder.setView(dialogView);
        ImageView loadingBG = dialogView.findViewById(R.id.ivLoaderBG);
        ImageView loader = dialogView.findViewById(R.id.ivLoader);

        ObjectAnimator animation = ObjectAnimator.ofFloat(loadingBG, "rotation", 0f, 360f);
        animation.setDuration(1000);
        animation.setRepeatCount(ObjectAnimator.INFINITE);
        animation.setInterpolator(new LinearInterpolator());
        animation.start();

        ObjectAnimator animation2 = ObjectAnimator.ofFloat(loader, "rotation", 0f, 360f);
        animation2.setDuration(1000);
        animation2.setRepeatCount(ObjectAnimator.INFINITE);
        animation2.setInterpolator(new LinearInterpolator());
        animation2.start();
        dialog = builder.create();
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    public static void hideProgressDialog(){
        dialog.dismiss();
    }

    public static void hideKeyboard(Activity activity) {
        try {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            //Find the currently focused view, so we can grab the correct window token from it.
            View view = activity.getCurrentFocus();
            //If no view currently has focus, create a new one, just so we can grab a window token from it
            if (view == null) {
                view = new View(activity);
            }
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception e) {
            Log.e("keyboard_hide_err", e.toString());
        }
    }

}
