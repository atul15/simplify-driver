package com.example.simplifydriver.HelperClasses;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefManager {
    private static SharedPrefManager mInstance;
    private static Context mCtx;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    private static final String SHARED_PREF_NAME = "SimplifyDriver";

    private static final String KEY_USER_ID = "keyuserid";
    private static final String KEY_USER_IMAGE = "keyuserimage";
    private static final String KEY_USER_NAME = "keyusername";
    private static final String KEY_USER_TYPE = "keyusertype";
    private static final String KEY_USER_MOBILE = "keyusermobile";
    private static final String KEY_USER_LOCATION_INTERVAL = "location_interval";
    private static final String KEY_USER_EMAIL = "keyuseremail";
    private static final String KEY_USER_ZONE= "keyuserzone";
    private static final String KEY_USER_TOKEN = "keyusertoken";
    private static final String KEY_TOTAL_DISTANCE = "keytotaldistance";

    private SharedPrefManager(Context context) {
        mCtx = context;
        sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public static synchronized SharedPrefManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedPrefManager(context);
        }
        return mInstance;
    }

    public void setUserName(String name) {
        editor.putString(KEY_USER_NAME, name);
        editor.apply();
    }

    public void setUserId(String id) {
        editor.putString(KEY_USER_ID, id);
        editor.apply();
    }

    public void setLocationInterval(Integer location_interval) {
        editor.putInt(KEY_USER_LOCATION_INTERVAL, location_interval);
        editor.apply();
    }

    public Integer getKeyUserLocationInterval(){
        return sharedPreferences.getInt(KEY_USER_LOCATION_INTERVAL, 10000);
    }


    public void setKeyTotalDistance(String distance) {
        editor.putString(KEY_TOTAL_DISTANCE, distance);
        editor.apply();
    }

    public String getKeyTotalDistance(){
        return sharedPreferences.getString(KEY_TOTAL_DISTANCE, "0");
    }

    public void setUserMobile(String mobile) {
        editor.putString(KEY_USER_MOBILE, mobile);
        editor.apply();
    }

    public String getUserMobile(){
        return sharedPreferences.getString(KEY_USER_MOBILE, null);
    }

    public void setUserEmail(String email) {
        editor.putString(KEY_USER_EMAIL, email);
        editor.apply();
    }

    public String getUserEmail(){
        return sharedPreferences.getString(KEY_USER_EMAIL, null);
    }

    public void setUserType(String type) {
        editor.putString(KEY_USER_TYPE, type);
        editor.apply();
    }

    public String getUserType(){
        return sharedPreferences.getString(KEY_USER_TYPE, null);
    }

    public void setUserZone(String zone) {
        editor.putString(KEY_USER_ZONE, zone);
        editor.apply();
    }

    public String getUserZone(){
        return sharedPreferences.getString(KEY_USER_ZONE, null);
    }

    public void setUserToken(String token) {
        editor.putString(KEY_USER_TOKEN, token);
        editor.apply();
    }

    public String getUserToken(){
        return sharedPreferences.getString(KEY_USER_TOKEN, null);
    }



    public void setKeyUserImage(String image) {
        editor.putString(KEY_USER_IMAGE, image);
        editor.apply();
    }

    public String getKeyUserImage(){
        return sharedPreferences.getString(KEY_USER_IMAGE, null);
    }

    public String getUserName(){
        return sharedPreferences.getString(KEY_USER_NAME, null);
    }

    public String getUserId(){
        return sharedPreferences.getString(KEY_USER_ID, null);
    }


    public boolean isLoggedIn() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        if (sharedPreferences.getString(KEY_USER_ID, null) != null)
            return true;
        return false;
    }


    public boolean logout() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
        return true;
    }


}
