package com.example.simplifydriver.HelperClasses;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

public class PermissionsClass {
    public static String[] appPermissions = {
            Manifest.permission.CAMERA
    };


    public static final int PERMISSIONS_REQUEST_CODE = 111;

    public static boolean checkAndRequestPermission(Activity activity){
        if (Build.VERSION.SDK_INT >= 23){
            try {
                List<String> listPermissionsNeeded = new ArrayList<>();
                for (String perm : appPermissions){
                    if (ContextCompat.checkSelfPermission(activity, perm) != PackageManager.PERMISSION_GRANTED){
                        listPermissionsNeeded.add(perm);
                    }
                }

                if (!listPermissionsNeeded.isEmpty()){
                    ActivityCompat.requestPermissions(activity,listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
                            PERMISSIONS_REQUEST_CODE);
                    return false;
                }

            }catch (RuntimeException re){ }

        }
        return true;
    }
}
