package com.example.simplifydriver.HelperClasses;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.simplifydriver.Models.OrderModel;
import com.google.gson.Gson;


public class SharedPreference2 {
    public static final String PREFS_NAME = "SimplifyDriver";
    public static final String SELECTED_ORDER = "SelectedOrder";
    OrderModel.OrderList orderSelected;

    public SharedPreference2() {
        super();
    }

    // THIS FOUR METHODS ARE USED FOR MAINTAINING PINNED.
    public void saveOrder(Context context, OrderModel.OrderList orderList) {

        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();

        Gson gson = new Gson();
        String jsonOrder = gson.toJson(orderList);

        editor.putString(SELECTED_ORDER, jsonOrder);
        editor.apply();

    }

    public void setStartedOrder(Context context, OrderModel.OrderList giftItem) {
            orderSelected = giftItem;
            saveOrder(context, orderSelected);
    }


    public OrderModel.OrderList getStartedOrder(Context context) {
        SharedPreferences settings;
        OrderModel.OrderList pinned;

        settings = context.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(SELECTED_ORDER)) {
            String jsonPinned = settings.getString(SELECTED_ORDER, null);
            Gson gson = new Gson();
            pinned = gson.fromJson(jsonPinned,
                    OrderModel.OrderList.class);
        } else {
            return null;
        }
        return pinned;
    }


}
