package com.example.simplifydriver.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.PopupMenu;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.simplifydriver.HelperClasses.ApplicationClass;
import com.example.simplifydriver.Models.NotificationModel;
import com.example.simplifydriver.R;

import java.util.ArrayList;

public class NotificationListAdapter extends RecyclerView.Adapter<NotificationListAdapter.ViewHolder> {
    ArrayList<NotificationModel.NotificationList> tripList;
    Context context;
    MenuClick menuClick;

    public interface MenuClick{
        void onMenuClick(int pos);
    }

    public NotificationListAdapter(ArrayList<NotificationModel.NotificationList> tripList, Context context, MenuClick menuClick) {
        this.tripList = tripList;
        this.context = context;
        this.menuClick = menuClick;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.list_item_notification, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        final NotificationModel.NotificationList model = tripList.get(position);

        holder.tvTitle.setText(model.getTitle());
        holder.tvDescription.setText(model.getBody());
        String date = ApplicationClass.convertTimeFormat("yyyy-MM-dd hh:mm", "dd MMM, yyyy hh:mm a", model.getDatetime());
        holder.tvTime.setText(date);


        holder.tvOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(context, holder.tvOptions);
                //inflating menu from xml resource
                popup.inflate(R.menu.delete_menu);
                //adding click listener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.delete:
                                menuClick.onMenuClick(position);
                                break;
                        }
                        return false;
                    }
                });
                //displaying the popup
                popup.show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return tripList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView tvTitle, tvDescription, tvTime, tvOptions;
        CardView cvBG;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvDescription = itemView.findViewById(R.id.tvDescription);
            tvTime = itemView.findViewById(R.id.tvTime);
            tvOptions = itemView.findViewById(R.id.tvOptions);
            cvBG = itemView.findViewById(R.id.cvBg);
        }
    }
}
