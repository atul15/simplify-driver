package com.example.simplifydriver.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.simplifydriver.R;

import java.util.ArrayList;

public class TimingSlotsAdapter extends RecyclerView.Adapter<TimingSlotsAdapter.ViewHolder>{
    ArrayList<String> adminItemList;
    Context context;
    ItemClickListener clickListener;
    int selectedPos = -1;

    public interface ItemClickListener {
        void onTimeSlotItemClick(int position);
    }

    public TimingSlotsAdapter(ArrayList<String> adminItemList, Context context, ItemClickListener clickListener) {
        this.adminItemList = adminItemList;
        this.context = context;
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.list_item_time_slot, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        if(selectedPos!=-1 && selectedPos==position){
            holder.clBg.setBackgroundColor(context.getColor(R.color.app_color_accent));
            holder.tvTitle.setTextColor(context.getColor(R.color.white));
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickListener.onTimeSlotItemClick(position);
                    selectedPos = position;
                }
            });
        }else {
            if(position<3){
                holder.clBg.setBackgroundColor(context.getColor(R.color.white));
                holder.tvTitle.setTextColor(context.getColor(R.color.grayBorder));
            }else {
                holder.clBg.setBackgroundColor(context.getColor(R.color.app_color));
                holder.tvTitle.setTextColor(context.getColor(R.color.white));
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        clickListener.onTimeSlotItemClick(position);
                        selectedPos = position;
                        notifyDataSetChanged();
                    }
                });
            }
        }

    }

    @Override
    public int getItemCount() {
        return adminItemList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView tvTitle;
        ConstraintLayout clBg;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.textView5);
            clBg = itemView.findViewById(R.id.clBg);
        }
    }
}
