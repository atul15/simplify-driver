package com.example.simplifydriver.Adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class ViewPagerAdapter extends FragmentPagerAdapter {
    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
//        switch (position)
//        {
//            case 0:
//                return new DieselFragment(); //ChildFragment1 at position 0
//            case 1:
//                return new PetrolFragment(); //ChildFragment2 at position 1
//            case 2:
//                return new OilFragment(); //ChildFragment3 at position 2
//        }
        return null; //does not happen
    }

    @Override
    public int getCount() {
        return 3; //three fragments
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if(position==0){
            return "Diesel";
        }else if(position ==1){
            return "Petrol";
        }else {
            return "Oil";
        }
    }
}
