package com.example.simplifydriver.Adapter;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Chronometer;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.simplifydriver.Activity.DashboardActivity;
import com.example.simplifydriver.Fragment.ReachedOrder;
import com.example.simplifydriver.HelperClasses.AppConstants;
import com.example.simplifydriver.HelperClasses.ApplicationClass;
import com.example.simplifydriver.HelperClasses.MyService;
import com.example.simplifydriver.HelperClasses.SharedPrefManager;
import com.example.simplifydriver.Models.LoginResponse;
import com.example.simplifydriver.Models.OrderModel;
import com.example.simplifydriver.Networking.ApiService;
import com.example.simplifydriver.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.ViewHolder>{
    ArrayList<OrderModel.OrderList> orderModelArrayList;
    Context context;
    ItemClickListener clickListener;

    public interface ItemClickListener {
        void onOrderItemClick(int position);
    }

    public OrderListAdapter(ArrayList<OrderModel.OrderList> orderModelArrayList, Context context, ItemClickListener clickListener) {
        this.orderModelArrayList = orderModelArrayList;
        this.context = context;
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.list_item_order, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        OrderModel.OrderList model = orderModelArrayList.get(position);

        holder.tvOrderId.setText(model.getOrder_no());
        holder.tvQty.setText(model.getQty()+" Ltr");
        holder.tvLoadingStatus.setText(model.getLoadingStatus()!=null?model.getLoadingStatus():"-");
        holder.tvRo.setText(model.getROName()!=null?model.getrOName():"-");
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.onOrderItemClick(position);
            }
        });

    }



    @Override
    public int getItemCount() {
        return orderModelArrayList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder{
        TextView tvOrderId, tvLoadingStatus, tvRo, tvQty;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvOrderId = itemView.findViewById(R.id.tvOrderId);
            tvLoadingStatus = itemView.findViewById(R.id.tvLoadingStatus);
            tvRo = itemView.findViewById(R.id.tvRo);
            tvQty = itemView.findViewById(R.id.tvQty);
        }
    }
}
