package com.example.simplifydriver.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class OrderModel {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("orderList")
    @Expose
    private ArrayList<OrderList> orderList = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<OrderList> getOrderList() {
        return orderList;
    }

    public void setOrderList(ArrayList<OrderList> orderList) {
        this.orderList = orderList;
    }


    public class OrderList {

        @SerializedName("order_id")
        @Expose
        private Integer orderId;
        @SerializedName("qty")
        @Expose
        private String qty;
        @SerializedName("order_no")
        @Expose
        private String order_no;
        @SerializedName("order_total_amount")
        @Expose
        private String order_total_amount;
        @SerializedName("RO_name")
        @Expose
        private String rOName;
        @SerializedName("RO_address")
        @Expose
        private String RoAddress;
        @SerializedName("delivery_address")
        @Expose
        private String deliveryAddress;
        @SerializedName("lat")
        @Expose
        private Double lat;
        @SerializedName("RO_longitude")
        @Expose
        private Double RO_longitude;
        @SerializedName("RO_latitute")
        @Expose
        private Double RO_latitute;
        @SerializedName("long")
        @Expose
        private Double _long;
        @SerializedName("vehical_no")
        @Expose
        private String vehicalNo;
        @SerializedName("customer_name")
        @Expose
        private String customerName;
        @SerializedName("customer_mobile")
        @Expose
        private String customerMobile;
        @SerializedName("payment_method")
        @Expose
        private String paymentMethod;
        @SerializedName("loading_status")
        @Expose
        private String loadingStatus;
        @SerializedName("loading_start_time")
        @Expose
        private String loadingStartTime;
        @SerializedName("loading_end_time")
        @Expose
        private String loadingEndTime;

        public String getRoAddress() {
            return RoAddress;
        }

        public void setRoAddress(String roAddress) {
            RoAddress = roAddress;
        }

        public Double getRO_longitude() {
            return RO_longitude;
        }

        public void setRO_longitude(Double RO_longitude) {
            this.RO_longitude = RO_longitude;
        }

        public Double getRO_latitute() {
            return RO_latitute;
        }

        public void setRO_latitute(Double RO_latitute) {
            this.RO_latitute = RO_latitute;
        }

        public Double get_long() {
            return _long;
        }

        public void set_long(Double _long) {
            this._long = _long;
        }

        public Integer getOrderId() {
            return orderId;
        }

        public void setOrderId(Integer orderId) {
            this.orderId = orderId;
        }

        public String getQty() {
            return qty;
        }

        public void setQty(String qty) {
            this.qty = qty;
        }

        public String getROName() {
            return rOName;
        }

        public void setROName(String rOName) {
            this.rOName = rOName;
        }

        public String getOrder_no() {
            return order_no;
        }

        public void setOrder_no(String order_no) {
            this.order_no = order_no;
        }

        public String getOrder_total_amount() {
            return order_total_amount;
        }

        public void setOrder_total_amount(String order_total_amount) {
            this.order_total_amount = order_total_amount;
        }

        public String getrOName() {
            return rOName;
        }

        public void setrOName(String rOName) {
            this.rOName = rOName;
        }

        public String getDeliveryAddress() {
            return deliveryAddress;
        }

        public void setDeliveryAddress(String deliveryAddress) {
            this.deliveryAddress = deliveryAddress;
        }

        public Double getLat() {
            return lat;
        }

        public void setLat(Double lat) {
            this.lat = lat;
        }

        public Double getLong() {
            return _long;
        }

        public void setLong(Double _long) {
            this._long = _long;
        }

        public String getVehicalNo() {
            return vehicalNo;
        }

        public void setVehicalNo(String vehicalNo) {
            this.vehicalNo = vehicalNo;
        }

        public String getCustomerName() {
            return customerName;
        }

        public void setCustomerName(String customerName) {
            this.customerName = customerName;
        }

        public String getCustomerMobile() {
            return customerMobile;
        }

        public void setCustomerMobile(String customerMobile) {
            this.customerMobile = customerMobile;
        }

        public String getPaymentMethod() {
            return paymentMethod;
        }

        public void setPaymentMethod(String paymentMethod) {
            this.paymentMethod = paymentMethod;
        }

        public String getLoadingStatus() {
            return loadingStatus;
        }

        public void setLoadingStatus(String loadingStatus) {
            this.loadingStatus = loadingStatus;
        }

        public String getLoadingStartTime() {
            return loadingStartTime;
        }

        public void setLoadingStartTime(String loadingStartTime) {
            this.loadingStartTime = loadingStartTime;
        }

        public String getLoadingEndTime() {
            return loadingEndTime;
        }

        public void setLoadingEndTime(String loadingEndTime) {
            this.loadingEndTime = loadingEndTime;
        }

    }


}
