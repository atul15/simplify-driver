package com.example.simplifydriver.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SettingsResponse {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("unseen_nt_count")
    @Expose
    private String unseen_nt_count;
    @SerializedName("admin_settings")
    @Expose
    private AdminSettings adminSettings;
    @SerializedName("user_data")
    @Expose
    private UserData userData;

    public String getUnseen_nt_count() {
        return unseen_nt_count;
    }

    public void setUnseen_nt_count(String unseen_nt_count) {
        this.unseen_nt_count = unseen_nt_count;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public AdminSettings getAdminSettings() {
        return adminSettings;
    }

    public void setAdminSettings(AdminSettings adminSettings) {
        this.adminSettings = adminSettings;
    }

    public UserData getUserData() {
        return userData;
    }

    public void setUserData(UserData userData) {
        this.userData = userData;
    }

    public class UserData {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("business")
        @Expose
        private String business;
        @SerializedName("street")
        @Expose
        private String street;
        @SerializedName("city")
        @Expose
        private String city;
        @SerializedName("state")
        @Expose
        private String state;
        @SerializedName("pincode")
        @Expose
        private String pincode;
        @SerializedName("GSTN")
        @Expose
        private String GSTN;
        @SerializedName("phone")
        @Expose
        private String phone;
        @SerializedName("profile_path")
        @Expose
        private String profile_path;
        @SerializedName("device_token")
        @Expose
        private String device_token;
        @SerializedName("wallet_limit")
        @Expose
        private String walletLimit;
        @SerializedName("sca_wallet_amount")
        @Expose
        private String scaWalletAmount;
        @SerializedName("wallet_amount")
        @Expose
        private String walletAmount;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getBusiness() {
            return business;
        }

        public void setBusiness(String business) {
            this.business = business;
        }

        public String getStreet() {
            return street;
        }

        public void setStreet(String street) {
            this.street = street;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getPincode() {
            return pincode;
        }

        public void setPincode(String pincode) {
            this.pincode = pincode;
        }

        public String getGSTN() {
            return GSTN;
        }

        public void setGSTN(String GSTN) {
            this.GSTN = GSTN;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getProfile_path() {
            return profile_path;
        }

        public void setProfile_path(String profile_path) {
            this.profile_path = profile_path;
        }

        public String getDevice_token() {
            return device_token;
        }

        public void setDevice_token(String device_token) {
            this.device_token = device_token;
        }

        public String getWalletLimit() {
            return walletLimit;
        }

        public void setWalletLimit(String walletLimit) {
            this.walletLimit = walletLimit;
        }

        public String getScaWalletAmount() {
            return scaWalletAmount;
        }

        public void setScaWalletAmount(String scaWalletAmount) {
            this.scaWalletAmount = scaWalletAmount;
        }

        public String getWalletAmount() {
            return walletAmount;
        }

        public void setWalletAmount(String walletAmount) {
            this.walletAmount = walletAmount;
        }
    }

    public class AdminSettings {

        @SerializedName("cost_per_ltr")
        @Expose
        private String costPerLtr;
        @SerializedName("exp_deliver_charge")
        @Expose
        private String expDeliverCharge;
        @SerializedName("last_cost_per_ltr")
        @Expose
        private String lastCostPerLtr;
        @SerializedName("gst_charge")
        @Expose
        private String gstCharge;
        @SerializedName("payment_charge")
        @Expose
        private String paymentCharge;
        @SerializedName("reward_equal_value")
        @Expose
        private String rewardEqualValue;
        @SerializedName("min_redeem_points")
        @Expose
        private String min_redeem_points;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("payment_gateway_charge")
        @Expose
        private String payment_gateway_charge;
        @SerializedName("phone")
        @Expose
        private String phone;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("order_cancellation_charge")
        @Expose
        private String order_cancellation_charge;
        @SerializedName("site_identity_icon")
        @Expose
        private String site_identity_icon;
        @SerializedName("logo")
        @Expose
        private String logo;
        @SerializedName("reward_min_qty")
        @Expose
        private String rewardMinQty;

        public String getMin_redeem_points() {
            return min_redeem_points;
        }

        public void setMin_redeem_points(String min_redeem_points) {
            this.min_redeem_points = min_redeem_points;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getPayment_gateway_charge() {
            return payment_gateway_charge;
        }

        public void setPayment_gateway_charge(String payment_gateway_charge) {
            this.payment_gateway_charge = payment_gateway_charge;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getOrder_cancellation_charge() {
            return order_cancellation_charge;
        }

        public void setOrder_cancellation_charge(String order_cancellation_charge) {
            this.order_cancellation_charge = order_cancellation_charge;
        }

        public String getSite_identity_icon() {
            return site_identity_icon;
        }

        public void setSite_identity_icon(String site_identity_icon) {
            this.site_identity_icon = site_identity_icon;
        }

        public String getRewardMinQty() {
            return rewardMinQty;
        }

        public void setRewardMinQty(String rewardMinQty) {
            this.rewardMinQty = rewardMinQty;
        }

        public String getCostPerLtr() {
            return costPerLtr;
        }

        public void setCostPerLtr(String costPerLtr) {
            this.costPerLtr = costPerLtr;
        }

        public String getExpDeliverCharge() {
            return expDeliverCharge;
        }

        public void setExpDeliverCharge(String expDeliverCharge) {
            this.expDeliverCharge = expDeliverCharge;
        }

        public String getLastCostPerLtr() {
            return lastCostPerLtr;
        }

        public void setLastCostPerLtr(String lastCostPerLtr) {
            this.lastCostPerLtr = lastCostPerLtr;
        }

        public String getGstCharge() {
            return gstCharge;
        }

        public void setGstCharge(String gstCharge) {
            this.gstCharge = gstCharge;
        }

        public String getPaymentCharge() {
            return paymentCharge;
        }

        public void setPaymentCharge(String paymentCharge) {
            this.paymentCharge = paymentCharge;
        }

        public String getRewardEqualValue() {
            return rewardEqualValue;
        }

        public void setRewardEqualValue(String rewardEqualValue) {
            this.rewardEqualValue = rewardEqualValue;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

    }

}
