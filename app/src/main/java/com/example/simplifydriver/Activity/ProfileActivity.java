package com.example.simplifydriver.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.simplifydriver.HelperClasses.SharedPrefManager;
import com.example.simplifydriver.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity {
    ImageView ivBack;
    TextView tvUserName, tvEmail, tvMobile;
    CircleImageView civUserImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ivBack = findViewById(R.id.ivBack);
        tvMobile = findViewById(R.id.tvMobile);
        tvEmail = findViewById(R.id.tvEmail);
        tvUserName = findViewById(R.id.tvUserName);
        civUserImage = findViewById(R.id.civUserImage);
        String image = SharedPrefManager.getInstance(this).getKeyUserImage();
        if(image!=null){
            Glide.with(this).load(image).into(civUserImage);
        }

        tvUserName.setText(SharedPrefManager.getInstance(this).getUserName());
        tvEmail.setText(SharedPrefManager.getInstance(this).getUserEmail());
        tvMobile.setText(SharedPrefManager.getInstance(this).getUserMobile());
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}