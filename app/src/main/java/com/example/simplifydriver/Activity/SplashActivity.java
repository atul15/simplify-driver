package com.example.simplifydriver.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.example.simplifydriver.HelperClasses.SharedPrefManager;
import com.example.simplifydriver.R;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(SharedPrefManager.getInstance(getApplicationContext()).isLoggedIn()){
                    startActivity(new Intent(SplashActivity.this, DashboardActivity.class));
                    finish();
                }else {
                    startActivity(new Intent(SplashActivity.this, LogInActivity.class));
                    finish();
                }

            }
        }, 1000);
    }
}