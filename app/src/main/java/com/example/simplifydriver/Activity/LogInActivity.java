package com.example.simplifydriver.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.simplifydriver.HelperClasses.ApplicationClass;
import com.example.simplifydriver.HelperClasses.SharedPrefManager;
import com.example.simplifydriver.Models.LoginResponse;
import com.example.simplifydriver.Networking.ApiService;
import com.example.simplifydriver.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LogInActivity extends AppCompatActivity {
    Button btnLogIn;
    String newToken;
    EditText etEmailMobile, etPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        btnLogIn = findViewById(R.id.btnLogIn);
        etEmailMobile = findViewById(R.id.etEmailMobile);
        etPassword = findViewById(R.id.etPassword);
        getToken();


        btnLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(newToken!=null){
                    login_user();
                }else {
                    getToken();
                }
            }
        });

    }

    private void getToken(){
        if (new ApplicationClass().isInternetAvailable(this)) {
            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(LogInActivity.this, new OnSuccessListener<InstanceIdResult>() {
                @Override
                public void onSuccess(InstanceIdResult instanceIdResult) {
                    newToken = instanceIdResult.getToken();
                    Log.e("newToken", newToken);
                }
            });
        }else {
            ApplicationClass.showAlertMsg(this, getString(R.string.no_internet));
        }
    }

    private void login_user(){
        String email = etEmailMobile.getText().toString().trim();
        String password = etPassword.getText().toString().trim();

        if (TextUtils.isEmpty(email)){
            etEmailMobile.setError("Please enter your email.");
            etEmailMobile.requestFocus();
        }else if (TextUtils.isEmpty(password)){
            etPassword.setError("Please enter your password.");
            etPassword.requestFocus();
        }
        else if(new ApplicationClass().isInternetAvailable(this)){
            ApplicationClass.showProgressDialog(LogInActivity.this);
            Call<LoginResponse> call = ApiService.createApiService().loginAction(email, password, newToken);
            call.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    ApplicationClass.hideProgressDialog();
                    try {
                        Toast.makeText(LogInActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        String id = response.body().getId().toString();
                        if (id!=null){
                            SharedPrefManager.getInstance(getApplicationContext()).setUserName(response.body().getName());
                            SharedPrefManager.getInstance(getApplicationContext()).setUserType(response.body().getRole());
                            SharedPrefManager.getInstance(getApplicationContext()).setUserId(response.body().getId().toString());
                            SharedPrefManager.getInstance(getApplicationContext()).setUserToken(response.body().getToken());
                            SharedPrefManager.getInstance(getApplicationContext()).setUserMobile(response.body().getPhone());
                            SharedPrefManager.getInstance(getApplicationContext()).setUserEmail(response.body().getEmail());
                            SharedPrefManager.getInstance(getApplicationContext()).setLocationInterval(response.body().getLocation_interval());
                            startActivity(new Intent(LogInActivity.this, DashboardActivity.class));
                            finish();
                        }
                    }catch (NullPointerException e){
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    ApplicationClass.hideProgressDialog();
                    Toast.makeText(LogInActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }else {
            Toast.makeText(this, "No internet connection.", Toast.LENGTH_SHORT).show();
        }
    }


}