package com.example.simplifydriver.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.simplifydriver.Adapter.NotificationListAdapter;
import com.example.simplifydriver.Adapter.OrderListAdapter;
import com.example.simplifydriver.Fragment.OrderList;
import com.example.simplifydriver.HelperClasses.ApplicationClass;
import com.example.simplifydriver.HelperClasses.SharedPrefManager;
import com.example.simplifydriver.Models.LoginResponse;
import com.example.simplifydriver.Models.NotificationModel;
import com.example.simplifydriver.Models.OrderModel;
import com.example.simplifydriver.Networking.ApiService;
import com.example.simplifydriver.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationActivity extends AppCompatActivity  implements NotificationListAdapter.MenuClick{
    RecyclerView rvNotifications;
    TextView tvNoData, tvOptions;
    NotificationListAdapter adapter;
    ArrayList<NotificationModel.NotificationList> notificationList;
    ImageView ivBack;
    ProgressBar progressBar;
    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        ivBack = findViewById(R.id.ivBack);
        rvNotifications = findViewById(R.id.rvNotification);
        tvNoData = findViewById(R.id.tvNoData);
        progressBar = findViewById(R.id.progressBar);
        tvOptions = findViewById(R.id.tvOptions);

        token = SharedPrefManager.getInstance(this).getUserToken();

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tvOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(NotificationActivity.this, tvOptions);
                //inflating menu from xml resource
                popup.inflate(R.menu.delete_menu);
                popup.getMenu().getItem(0).setVisible(false);
                popup.getMenu().getItem(1).setVisible(true);
                //adding click listener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.deleteAll:
                                if(notificationList!=null){
                                    if(notificationList.size()!=0){
                                        deleteNotifications("", 0);
                                    }
                                }
                                break;
                        }
                        return false;
                    }
                });
                //displaying the popup
                popup.show();
            }
        });

    }

    public void deleteNotifications( String ids, int pos) {
        if (new ApplicationClass().isInternetAvailable(NotificationActivity.this)) {
            progressBar.setVisibility(View.VISIBLE);
            String userId = SharedPrefManager.getInstance(NotificationActivity.this).getUserId();
            Call<LoginResponse> call = ApiService.createApiService().deleteNotifications(userId, ids, "Bearer "+token);
            call.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    try {
                        progressBar.setVisibility(View.GONE);
                        if (response.body().getStatus()==1) {
                            if(ids.equals("")){
                                notificationList.clear();
                                setData();
                            }else {
                                notificationList.remove(pos);
                                adapter.notifyItemRemoved(pos);
                                adapter.notifyItemRangeChanged(pos, notificationList.size());
                            }
                        }else {
                            ApplicationClass.showAlertMsg(NotificationActivity.this, response.body().getMessage());
                        }

                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    Toast.makeText(NotificationActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);
                }
            });
        } else {
            Toast.makeText(NotificationActivity.this, "No internet connection.", Toast.LENGTH_SHORT).show();
        }
    }



    private void getNotificationList(String userId, String token) {
        if (new ApplicationClass().isInternetAvailable(NotificationActivity.this)) {
            progressBar.setVisibility(View.VISIBLE);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            String currentDateandTime = sdf.format(new Date());
            Call<NotificationModel> call = ApiService.createApiService().getNotificationList(userId, currentDateandTime,"Bearer " + token);
            call.enqueue(new Callback<NotificationModel>() {
                @Override
                public void onResponse(Call<NotificationModel> call, Response<NotificationModel> response) {
                    try {
                        progressBar.setVisibility(View.GONE);
                        notificationList = new ArrayList<>();
                        notificationList = response.body().getNotificationList();
                        setData();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<NotificationModel> call, Throwable t) {
                    Toast.makeText(NotificationActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);
                }
            });
        } else {
            Toast.makeText(NotificationActivity.this, "No internet connection.", Toast.LENGTH_SHORT).show();
        }
    }

    private void setData() {
        if (notificationList.size() > 0) {
            adapter = new NotificationListAdapter(notificationList, NotificationActivity.this, this);
            tvNoData.setVisibility(View.GONE);
            rvNotifications.setVisibility(View.VISIBLE);
            rvNotifications.setAdapter(adapter);
        } else {
            tvNoData.setVisibility(View.VISIBLE);
            rvNotifications.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getNotificationList(
                SharedPrefManager.getInstance(NotificationActivity.this).getUserId(),
                SharedPrefManager.getInstance(NotificationActivity.this).getUserToken());
    }

    @Override
    public void onMenuClick(int pos) {
        deleteNotifications(notificationList.get(pos).getId(), pos);
    }

}