package com.example.simplifydriver.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.simplifydriver.Fragment.MapFragment;
import com.example.simplifydriver.Fragment.OrderList;
import com.example.simplifydriver.HelperClasses.AppConstants;
import com.example.simplifydriver.HelperClasses.ApplicationClass;
import com.example.simplifydriver.HelperClasses.MyService;
import com.example.simplifydriver.HelperClasses.SharedPrefManager;
import com.example.simplifydriver.HelperClasses.SharedPreference2;
import com.example.simplifydriver.Models.SettingsResponse;
import com.example.simplifydriver.Networking.ApiService;
import com.example.simplifydriver.R;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardActivity extends AppCompatActivity {
    DrawerLayout drawer;
    ConstraintLayout clLogout, clProfile;
    ImageView ivClose, ivNotification;
    ApplicationClass applicationClass;
    public Location userLocation;
    public Location userLastLocation;
    public String address = "";
    FusedLocationProviderClient fusedClient;
    ProgressDialog dialog;
    private LocationRequest mRequest;
    private LocationCallback mCallback;
    CircleImageView userImage;
    TextView userName, userEmail, tvNotificationCount;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawer = findViewById(R.id.drawer_layout);
        applicationClass = new ApplicationClass();
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        //drawer.setDrawerListener(toggle);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        clLogout = findViewById(R.id.clLogout);
        clProfile = findViewById(R.id.clProfile);
        ivClose = findViewById(R.id.ivClose);
        ivNotification = findViewById(R.id.ivNotification);
        userImage = findViewById(R.id.userImage);
        userName = findViewById(R.id.userName);
        userEmail = findViewById(R.id.userEmail);
        tvNotificationCount = findViewById(R.id.tvNotificationCount);

        ivNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DashboardActivity.this, NotificationActivity.class));
            }
        });
        clProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DashboardActivity.this, ProfileActivity.class));
            }
        });
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        clLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent serviceIntent = new Intent(getApplicationContext(), MyService.class);
                if (Build.VERSION.SDK_INT >= 26) {
                    if (MyService.isInstanceCreated()) {
                        stopService(serviceIntent);
                    }
                }
                new SharedPreference2().setStartedOrder(getApplicationContext(), null);
                Intent intent = new Intent(getApplicationContext(), LogInActivity.class);
                startActivity(intent);
                finish();
                SharedPrefManager.getInstance(DashboardActivity.this).logout();
            }
        });

        dialog = new ProgressDialog(this);
        getUserLocation();

    }


    public void setUserDetail(){
        String image = SharedPrefManager.getInstance(this).getKeyUserImage();
        if(image!=null){
            Glide.with(this).load(image).into(userImage);
        }
        userName.setText(SharedPrefManager.getInstance(this).getUserName());
        userEmail.setText(SharedPrefManager.getInstance(this).getUserEmail());
    }

    private void getUserLocation() {
        dialog.setTitle("We are getting your location");
        dialog.setMessage("Turn on GPS and wait...");
        dialog.setCancelable(false);
        fusedClient = LocationServices.getFusedLocationProviderClient(this);
        mCallback = new LocationCallback() {
            //This callback is where we get "streaming" location updates. We can check things like accuracy to determine whether
            //this latest update should replace our previous estimate.
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    Log.d("TAG", "locationResult null");
                    return;
                }
                Log.d("TAG", "received " + locationResult.getLocations().size() + " locations");
                for (Location loc : locationResult.getLocations()) {
                    dialog.dismiss();
                    userLocation = loc;
                    Log.e("Location", loc.getLatitude() + " " + loc.getLongitude());
                    address = getAddress(loc.getLatitude(), loc.getLongitude());

                    if(userLastLocation==null){
                        if(new SharedPreference2().getStartedOrder(DashboardActivity.this)==null){
                            loadFragment(new OrderList());
                        }else {
                            loadFragment(new MapFragment());
//                            if (fusedClient != null) {
//                                fusedClient.removeLocationUpdates(mCallback);
//                            }
                        }
                    }


                    userLastLocation = userLocation;
//                    OrderBottomSheet bottomSheet = new OrderBottomSheet();
//                    bottomSheet.show(getChildFragmentManager(), "OrderBottomSheet");

                }
            }

            @Override
            public void onLocationAvailability(LocationAvailability locationAvailability) {
                Log.d("TAG", "locationAvailability is " + locationAvailability.isLocationAvailable());
                super.onLocationAvailability(locationAvailability);
            }
        };

        //permissions
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            //request permission.
            //However check if we need to show an explanatory UI first
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                showRationale();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_NETWORK_STATE}, 222);
            }
        } else {
            //we already have the permission. Do any location wizardry now
            if (!dialog.isShowing()) {
                dialog.show();
            }

        }
    }

    @SuppressLint("MissingPermission")
    private void locationWizardry() {
        //Initially, get last known location. We can refine this estimate later
        fusedClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    String loc = location.getProvider() + ":Accu:(" + location.getAccuracy() + "). Lat:" + location.getLatitude() + ",Lon:" + location.getLongitude();
                    Log.e("locaitonWizardry", loc);
                    //getAddress(location.getLatitude(), location.getLongitude());
                }
            }
        });


        //now for receiving constant location updates:
        createLocRequest();
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mRequest);

        //This checks whether the GPS mode (high accuracy,battery saving, device only) is set appropriately for "mRequest". If the current settings cannot fulfil
        //mRequest(the Google Fused Location Provider determines these automatically), then we listen for failutes and show a dialog box for the user to easily
        //change these settings.
        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());
        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    // Location settings are not satisfied, but this can be fixed
                    // by showing the user a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(DashboardActivity.this, 500);
                    } catch (IntentSender.SendIntentException sendEx) {
                        // Ignore the error.
                    }
                }
            }
        });

        //actually start listening for updates: See on Resume(). It's done there so that conveniently we can stop listening in onPause
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 222: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Toast.makeText(this, "Thanks bud", Toast.LENGTH_SHORT).show();
                    if (!dialog.isShowing()) {
                        dialog.show();
                    }
                    locationWizardry();
                } else {
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
                }
            }

        }
    }

    private void getAllSettings(String token) {
        if (new ApplicationClass().isInternetAvailable(DashboardActivity.this)) {
            Call<SettingsResponse> call = ApiService.createApiService().getAllSettings("Bearer " + token);
            call.enqueue(new Callback<SettingsResponse>() {
                @Override
                public void onResponse(Call<SettingsResponse> call, Response<SettingsResponse> response) {
                    try {
                        if(response.body().getUserData().getDevice_token()==null){
                            AlertDialog.Builder builder = new AlertDialog.Builder(DashboardActivity.this);
                            builder.setMessage("Your session was lost, please login again!!")
                                    .setCancelable(false)
                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        public void onClick(final DialogInterface dialog, final int id) {
                                            dialog.cancel();
                                            Intent intent = new Intent(getApplicationContext(), LogInActivity.class);
                                            startActivity(intent);
                                            finish();
                                            SharedPrefManager.getInstance(DashboardActivity.this).logout();
                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();
                            return;
                        }
                        if (response.body().getUserData().getId() != null) {
                            SharedPrefManager.getInstance(getApplicationContext()).setUserName(response.body().getUserData().getName());
                            if(response.body().getUserData().getProfile_path()!=null){
                                SharedPrefManager.getInstance(getApplicationContext()).setKeyUserImage(response.body().getUserData().getProfile_path());
                            }
                            if(response.body().getUnseen_nt_count().equals("0")){
                                tvNotificationCount.setVisibility(View.GONE);
                            }else {
                                tvNotificationCount.setVisibility(View.VISIBLE);
                                tvNotificationCount.setText(response.body().getUnseen_nt_count());
                            }
                            SharedPrefManager.getInstance(getApplicationContext()).setUserId(response.body().getUserData().getId().toString());
                            SharedPrefManager.getInstance(getApplicationContext()).setUserMobile(response.body().getUserData().getPhone());
                            SharedPrefManager.getInstance(getApplicationContext()).setUserEmail(response.body().getUserData().getEmail());
                        }
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<SettingsResponse> call, Throwable t) {
                    //Toast.makeText(DashboardActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(DashboardActivity.this, "No internet connection.", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(
                    Settings.ACTION_WIFI_SETTINGS));
        }
    }


    public String getAddress(double lat, double lng) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        String location = "Not found";
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            Address obj = addresses.get(0);
            String add = obj.getAddressLine(0);

            location = add;
            //Toast.makeText(getContext(), location, Toast.LENGTH_SHORT).show();
//            if (fusedClient != null) {
//                fusedClient.removeLocationUpdates(mCallback);
//            }

        } catch (IOException e) {
            e.printStackTrace();

        }
        return location;
    }

    @SuppressLint("MissingPermission")
    protected void startLocationUpdates() {
        fusedClient.requestLocationUpdates(mRequest, mCallback, null);
    }


    protected void createLocRequest() {
        mRequest = new LocationRequest();
        mRequest.setInterval(30000);//time in ms; every ~30 seconds
        mRequest.setFastestInterval(30000);
        mRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }


    private void showRationale() {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle(R.string.title_location_permission)
                .setCancelable(false)
                .setMessage(R.string.text_location_permission)
                .setPositiveButton("Sure", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(DashboardActivity.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_NETWORK_STATE}, 222);
                        dialog.dismiss();
                    }
                })
                .create();
        dialog.show();
    }

    public void loadFragment(Fragment fragment){
        String tag = "";
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                .add(R.id.fragment_container_view, fragment, tag)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if(getSupportFragmentManager().getFragments().size()==2 || getSupportFragmentManager().getFragments().size()==1){
                finish();
            }else {
                super.onBackPressed();
            }

        }
    }

    public void removeFragments(Fragment fragmentToLoad){
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            if (fragment != null) {
                getSupportFragmentManager().beginTransaction().remove(fragment).commit();
            }
        }
        if(fragmentToLoad!=null){
            loadFragment(fragmentToLoad);
        }
    }

    public void showAlertMsgWithFinish(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                        Intent serviceIntent = new Intent(getApplicationContext(), MyService.class);
                        if (Build.VERSION.SDK_INT >= 26) {
                            if (MyService.isInstanceCreated()) {
                                getApplicationContext().stopService(serviceIntent);
                            }
                        }
                        new SharedPreference2().setStartedOrder(getApplicationContext(), null);
                        removeFragments(new OrderList());
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUserDetail();
        getAllSettings(SharedPrefManager.getInstance(DashboardActivity.this).getUserToken());
        if(!applicationClass.isInternetAvailable(DashboardActivity.this)){
            ApplicationClass.showAlertMsg(DashboardActivity.this, "Please connect to internet!");
        }
        locationWizardry();
        if (userLocation==null) {
            startLocationUpdates();
        } else {
            dialog.dismiss();
        }
    }
}