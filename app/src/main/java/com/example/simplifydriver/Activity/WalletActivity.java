package com.example.simplifydriver.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.simplifydriver.R;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONObject;

public class WalletActivity extends AppCompatActivity implements PaymentResultListener {
    ImageView ivBack;
    Button btnProceed;
    EditText etAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);
        ivBack = findViewById(R.id.ivBack);
        btnProceed = findViewById(R.id.btnProceed);
        etAmount = findViewById(R.id.etAmount);
        Checkout.preload(getApplicationContext());


        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etAmount.getText().toString().length()>1) {
                    startPayment();
                } else {
                    Toast.makeText(WalletActivity.this, "Enter amount to add.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void startPayment() {
        final Checkout checkout = new Checkout();
        checkout.setKeyID(getString(R.string.razor_key_id));

        checkout.setImage(R.drawable.logo_1);
        final Activity activity = this;
        try {
            JSONObject options = new JSONObject();

            options.put("name", "Simplify Services");
            options.put("description", "Add Money to Wallet");
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
            //options.put("order_id", "order_DBJOWzybf0sJbb");//from response of step 3.
            options.put("theme.color", "#132644");
            options.put("currency", "INR");
            String payment = etAmount.getText().toString().trim();
            // amount is in paise so please multiple it by 100
            //Payment failed Invalid amount (should be passed in integer paise. Minimum value is 100 paise, i.e. ₹ 1)
            double total = Double.parseDouble(payment);
            total = total * 100;
            options.put("amount", total);//pass amount in currency subunits
//            options.put("prefill.email", "vandanasinghvs024@gmail.com");
//            options.put("prefill.contact","1234567897");
            checkout.open(activity, options);

        } catch(Exception e) {
            Log.e("startPayment", "Error in starting Razorpay Checkout", e);
        }
    }

    @Override
    public void onPaymentSuccess(String s) {
        try {
            Toast.makeText(this, "Payment Successful: " + s, Toast.LENGTH_SHORT).show();
            Log.e("onPaymentSuccess", "onPaymentSuccess "+ s);
        } catch (Exception e) {
            Log.e("onPaymentSuccess", "Exception in onPaymentSuccess", e);
        }
    }

    @Override
    public void onPaymentError(int i, String s) {
        try {
            Toast.makeText(this, "Payment failed: " + i + " " + s, Toast.LENGTH_SHORT).show();
            Log.e("onPaymentError", "onPaymentError"+s+" code "+i);
        } catch (Exception e) {
            Log.e("onPaymentError", "Exception in onPaymentError", e);
        }
    }
}