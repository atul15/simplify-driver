package com.example.simplifydriver.Fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.example.simplifydriver.Activity.DashboardActivity;
import com.example.simplifydriver.HelperClasses.AppConstants;
import com.example.simplifydriver.HelperClasses.ApplicationClass;
import com.example.simplifydriver.HelperClasses.DirectionsJSONParser;
import com.example.simplifydriver.HelperClasses.MyService;
import com.example.simplifydriver.HelperClasses.SharedPrefManager;
import com.example.simplifydriver.HelperClasses.SharedPreference2;
import com.example.simplifydriver.R;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class MapFragment extends Fragment implements OnMapReadyCallback {
    private GoogleMap mMap;
    SupportMapFragment mapFragment;
    Double desLat = null;
    Double desLong = null;
    ConstraintLayout clOrder;
    Button btnReached;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_map, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        clOrder = view.findViewById(R.id.clOrder);
        btnReached = view.findViewById(R.id.btnReached);

        if(AppConstants.orderModelSelected!=null){
            new SharedPreference2().setStartedOrder(getContext(), AppConstants.orderModelSelected);
        }else {
            AppConstants.orderModelSelected = new SharedPreference2().getStartedOrder(getContext());
        }

        btnReached.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float[] results = new float[1];
                Location.distanceBetween(((DashboardActivity) getActivity()).userLocation.getLatitude(), ((DashboardActivity) getActivity()).userLocation.getLongitude(),
                        AppConstants.orderModelSelected.getLat(), AppConstants.orderModelSelected.getLong(), results);
                Log.e("LocationDistance", results[0] + "");
                if (results[0] < 200) {
                    ((DashboardActivity) getActivity()).loadFragment(new ReachedOrder());
                } else {
                    ApplicationClass.showAlertMsg(getContext(), "Oops!! You are "+results[0]+"(meter) away from destination.");
                }

            }
        });
        if(((DashboardActivity)getActivity()).userLocation!=null){
            mapFragment.getMapAsync(this);
        }

    }


    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMyLocationEnabled(true);
        desLat = AppConstants.orderModelSelected.getLat();
        desLong = AppConstants.orderModelSelected.getLong();
        int height = 120;
        int width = 120;
        BitmapDrawable bitmapdraw = (BitmapDrawable) ActivityCompat.getDrawable(getContext(), R.drawable.pin);
        Bitmap b = bitmapdraw.getBitmap();
        Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(((DashboardActivity) getActivity()).userLocation.getLatitude(), ((DashboardActivity) getActivity()).userLocation.getLongitude()))
                .title(((DashboardActivity) getActivity()).address)
                .icon(BitmapDescriptorFactory.fromBitmap(smallMarker))
        );
        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(desLat, desLong))
                .title(AppConstants.orderModelSelected.getDeliveryAddress())
        );


        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(((DashboardActivity) getActivity()).userLocation.getLatitude(), ((DashboardActivity) getActivity()).userLocation.getLongitude()), 10));
        ArrayList<LatLng> latLngs = new ArrayList<>();
        latLngs.add(new LatLng(((DashboardActivity) getActivity()).userLocation.getLatitude(), ((DashboardActivity) getActivity()).userLocation.getLongitude()));
        latLngs.add(new LatLng(desLat, desLong));

        List<String> urls = getDirectionsUrl(latLngs);
        if (urls.size() > 1) {
            for (int i = 0; i < urls.size(); i++) {
                String url = urls.get(i);
                DownloadTask downloadTask = new DownloadTask();
                // Start downloading json data from Google Directions API
                downloadTask.execute(url);
            }
        }

        new ApplicationClass().setCurrentActivity(getActivity());

    }


    /**
     * A class to download data from Google Directions URL
     */
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                //Log.e("DownloadTask",data);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    /**
     * A class to parse the Google Directions in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                routes = parser.parse(jObject);

                JSONArray routeArray = jObject.getJSONArray("routes");
                JSONObject routesObject = routeArray.getJSONObject(0);
                JSONArray legsArray = routesObject.getJSONArray("legs");
                JSONObject legs = legsArray.getJSONObject(0);
                JSONObject distanceObj = legs.getJSONObject("distance");

                //here is your distance
                String parsedDistance = distanceObj.getString("text");
                SharedPrefManager.getInstance(getContext()).setKeyTotalDistance(parsedDistance.replace("km", "").trim());
                Intent serviceIntent = new Intent(getActivity().getApplicationContext(), MyService.class);
                if (Build.VERSION.SDK_INT >= 26) {
                    if (!MyService.isInstanceCreated()) {
                        getActivity().getApplicationContext().startForegroundService(serviceIntent);
                    }
                } else {
                    ApplicationClass.showAlertMsg(getContext(), "Unable to start service. Use Android 8 or above.");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = new ArrayList<LatLng>();
            PolylineOptions lineOptions = new PolylineOptions();
            lineOptions.width(12);
            lineOptions.color(getActivity().getColor(R.color.app_color));
            MarkerOptions markerOptions = new MarkerOptions();

            for (int i = 0; i < result.size(); i++) {

                List<HashMap<String, String>> path = result.get(i);

                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }
                lineOptions.addAll(points);

            }

            // Drawing polyline in the Google Map for the i-th route
            if (points.size() != 0) mMap.addPolyline(lineOptions);
        }
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.connect();

            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }


    private List<String> getDirectionsUrl(ArrayList<LatLng> markerPoints) {
        List<String> mUrls = new ArrayList<>();
        if (markerPoints.size() > 1) {
            String str_origin = markerPoints.get(0).latitude + "," + markerPoints.get(0).longitude;
            String str_dest = markerPoints.get(1).latitude + "," + markerPoints.get(1).longitude;

            String sensor = "sensor=false";
            String key = "key=" + getString(R.string.maps_api_key);
            String parameters = "origin=" + str_origin + "&destination=" + str_dest + "&" + key;
            String output = "json";
            String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

            mUrls.add(url);
            for (int i = 0; i < markerPoints.size(); i++)//loop starts from 2 because 0 and 1 are already printed
            {
                str_origin = str_dest;
                str_dest = markerPoints.get(i).latitude + "," + markerPoints.get(i).longitude;
                parameters = "origin=" + str_origin + "&destination=" + str_dest + "&" + key;
                url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
                mUrls.add(url);
            }
        }

        return mUrls;
    }

}