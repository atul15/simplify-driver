package com.example.simplifydriver.Fragment;

import android.location.Location;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.Fragment;

import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.simplifydriver.Activity.DashboardActivity;
import com.example.simplifydriver.HelperClasses.AppConstants;
import com.example.simplifydriver.HelperClasses.ApplicationClass;
import com.example.simplifydriver.HelperClasses.SharedPrefManager;
import com.example.simplifydriver.HelperClasses.SharedPreference2;
import com.example.simplifydriver.Models.LoginResponse;
import com.example.simplifydriver.Networking.ApiService;
import com.example.simplifydriver.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class OrderDetail extends Fragment {
    ImageView ivBack;
    Button btnStartTrip;
    TextView tvOrderId, tvQty, tvRoName, tvLat, tvLong, tvDriverName, tvDriverMobile, tvVehicleNumber, tvDeliveryAddress, tvCustomerName, tvCustomerMobile;
    Chronometer chronometer;
    ProgressBar progressBarLoading;
    SwitchCompat loadSwitch;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_order_detail, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ivBack = view.findViewById(R.id.ivBack);
        btnStartTrip = view.findViewById(R.id.btnStartTrip);
        tvOrderId = view.findViewById(R.id.tvOrderId);
        tvQty = view.findViewById(R.id.tvQty);
        tvRoName = view.findViewById(R.id.tvRoName);
        tvLat = view.findViewById(R.id.tvLat);
        tvLong = view.findViewById(R.id.tvLong);
        tvDriverName = view.findViewById(R.id.tvDriverName);
        tvDriverMobile = view.findViewById(R.id.tvDriverMobile);
        tvVehicleNumber = view.findViewById(R.id.tvVehicleNumber);
        tvDeliveryAddress = view.findViewById(R.id.tvDeliveryAddress);
        tvCustomerName = view.findViewById(R.id.tvCustomerName);
        tvCustomerMobile = view.findViewById(R.id.tvCustomerMobile);
        chronometer = view.findViewById(R.id.chronometer);
        progressBarLoading = view.findViewById(R.id.progressBar);
        loadSwitch = view.findViewById(R.id.loadSwitch);

        if(AppConstants.orderModelSelected ==null){
            new ApplicationClass().restartApp(getContext());
            return;
        }

        tvOrderId.setText(AppConstants.orderModelSelected.getOrder_no());
        tvQty.setText(AppConstants.orderModelSelected.getQty()+" Ltr");
        tvRoName.setText(AppConstants.orderModelSelected.getROName());
        tvLat.setText(AppConstants.orderModelSelected.getRoAddress()!=null?AppConstants.orderModelSelected.getRoAddress():"-");
        tvLong.setText(AppConstants.orderModelSelected.getRO_longitude()!=null?AppConstants.orderModelSelected.getRO_longitude().toString():"-");
        tvDriverName.setText(SharedPrefManager.getInstance(getContext()).getUserName());
        tvDriverMobile.setText(SharedPrefManager.getInstance(getContext()).getUserMobile());
        tvVehicleNumber.setText(AppConstants.orderModelSelected.getVehicalNo());
        tvDeliveryAddress.setText(AppConstants.orderModelSelected.getDeliveryAddress());
        tvCustomerName.setText(AppConstants.orderModelSelected.getCustomerName());
        tvCustomerMobile.setText(AppConstants.orderModelSelected.getCustomerMobile());

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        btnStartTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(AppConstants.orderModelSelected.getLoadingStatus().equals("loaded")){
                    if (AppConstants.orderModelSelected.getLat()==null||AppConstants.orderModelSelected.getLong()==null){
                        ApplicationClass.showAlertMsg(getContext(), "No address point to start trip.");
                    }else {
                        startTrip(AppConstants.orderModelSelected.getOrderId().toString(), SharedPrefManager.getInstance(getContext()).getUserToken());
                    }

                }else {
                    ApplicationClass.showAlertMsg(getContext(), "This order is not loaded.");
                }

            }
        });
        chronometer.setText(AppConstants.orderModelSelected.getLoadingStatus());
        if(AppConstants.orderModelSelected.getLoadingStatus().equals("loaded")){
            loadSwitch.setChecked(true);
            loadSwitch.setClickable(false);
        }
    }


    private void updateLoadingTime(final Integer order_id, String loading_start_time, final String loading_end_time, final String token) {
        if (new ApplicationClass().isInternetAvailable(getContext())) {
            Call<LoginResponse> call = ApiService.createApiService().orderLoadingStatus(order_id.toString(), loading_start_time, loading_end_time, "Bearer " + token);
            call.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    try {
                        progressBarLoading.setVisibility(View.GONE);
                        Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        if (response.body().getStatus()==1) {
                            if (!loading_start_time.equals("")) {
                                chronometer.setBase(SystemClock.elapsedRealtime());
                                chronometer.start();
                            } else {
                                chronometer.stop();
                                AppConstants.orderModelSelected.setLoadingStatus("loaded");
                                chronometer.setText("loaded");
                                loadSwitch.setClickable(false);
                            }
                        }
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    Log.e("TripSiteLocation", "onResponse: " + t.getMessage());
                    Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(getContext(), "No internet connection.", Toast.LENGTH_SHORT).show();
        }
    }

    private void startTrip(String orderId, String token){
        if(new ApplicationClass().isInternetAvailable(getContext())){
            ApplicationClass.showProgressDialog(getActivity());
            Call<LoginResponse> call = ApiService.createApiService().startTrip(orderId, "Bearer "+token);
            call.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    ApplicationClass.hideProgressDialog();
                    try {
                        if(response.body().getStatus()==1){
                            Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            ((DashboardActivity)getActivity()).removeFragments(new MapFragment());
                        }else {
                            ApplicationClass.showAlertMsg(getContext(), response.body().getMessage());
                        }

                    }catch (NullPointerException e){
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    ApplicationClass.hideProgressDialog();
                    Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }else {
            Toast.makeText(getContext(), "No internet connection.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(((DashboardActivity)getActivity()).userLocation!=null){
            if(AppConstants.orderModelSelected.getRO_latitute()==null || AppConstants.orderModelSelected.getRO_longitude()==null){
                Toast.makeText(getContext(), "RO Location not available, contact admin.", Toast.LENGTH_SHORT).show();
                loadSwitch.setClickable(false);
            }else {
                float[] results = new float[1];
                Location.distanceBetween(((DashboardActivity)getActivity()).userLocation.getLatitude(), ((DashboardActivity)getActivity()).userLocation.getLongitude(),
                        AppConstants.orderModelSelected.getRO_latitute(), AppConstants.orderModelSelected.getRO_longitude(), results);
                if (results[0] <= 50) {
                    if(AppConstants.orderModelSelected.getLoadingStatus().equals("loaded")){
                        loadSwitch.setClickable(false);
                    }else {
                        loadSwitch.setClickable(true);
                    }
                    loadSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            if (isChecked) {
                                progressBarLoading.setVisibility(View.VISIBLE);
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                                String currentDateandTime = sdf.format(new Date());
                                updateLoadingTime(AppConstants.orderModelSelected.getOrderId(),
                                        currentDateandTime, "", SharedPrefManager.getInstance(getContext()).getUserToken());
                            } else {
                                progressBarLoading.setVisibility(View.VISIBLE);
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                                String currentDateandTime = sdf.format(new Date());
                                updateLoadingTime(AppConstants.orderModelSelected.getOrderId(), "",
                                        currentDateandTime, SharedPrefManager.getInstance(getContext()).getUserToken());
                            }
                        }
                    });
                } else {
                    loadSwitch.setClickable(false);
                    ApplicationClass.showAlertMsg(getContext(),"Oops!! You are not around RO Location. You are "+results[0]+"(meter) away from RO.");
                }
            }
        }
    }
}