package com.example.simplifydriver.Fragment;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.example.simplifydriver.Activity.DashboardActivity;
import com.example.simplifydriver.BuildConfig;
import com.example.simplifydriver.HelperClasses.AppConstants;
import com.example.simplifydriver.HelperClasses.ApplicationClass;
import com.example.simplifydriver.HelperClasses.PermissionsClass;
import com.example.simplifydriver.HelperClasses.SharedPrefManager;
import com.example.simplifydriver.Models.LoginResponse;
import com.example.simplifydriver.Networking.ApiService;
import com.example.simplifydriver.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class ReachedOrder extends Fragment {
    private File output = null;
    private Uri fileUri;
    private static final int CAMERA_PIC_REQUEST_1 = 101;
    private static final int CAMERA_PIC_REQUEST_2 = 102;
    private String FILE_NAME;
    ImageView ivCamera, ivCamera2;
    ImageView ivBack;
    TextView tvOrderId, tvAmount, tvQty;
    RadioGroup rgOrderStatus, rgPaymentMethod, radioGroup3;
    Button btnSubmit;
    File image1, image2;
    String status;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_reached_order, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ivCamera = view.findViewById(R.id.ivCamera);
        ivCamera2 = view.findViewById(R.id.ivCamera2);
        ivBack = view.findViewById(R.id.ivBack);
        rgOrderStatus = view.findViewById(R.id.rgOrderStatus);
        rgPaymentMethod = view.findViewById(R.id.rgPaymentMethod);
        radioGroup3 = view.findViewById(R.id.radioGroup3);
        tvOrderId = view.findViewById(R.id.tvOrderId);
        tvAmount = view.findViewById(R.id.tvAmount);
        tvQty = view.findViewById(R.id.tvQty);
        btnSubmit = view.findViewById(R.id.btnSubmit);
        RadioButton payment3 = view.findViewById(R.id.payment3);
        RadioButton payment2 = view.findViewById(R.id.payment2);
        RadioButton payment1 = view.findViewById(R.id.payment1);

        tvQty.setText(AppConstants.orderModelSelected.getQty() + " Ltr.");
        tvAmount.setText(AppConstants.orderModelSelected.getOrder_total_amount()+" ₹");
        tvOrderId.setText("Order No: "+AppConstants.orderModelSelected.getOrder_no());
        String paymentMethod = AppConstants.orderModelSelected.getPaymentMethod();
        radioGroup3.setActivated(false);
        rgPaymentMethod.setActivated(false);
        if (paymentMethod.equals("sca")) {
            payment3.setChecked(true);
            payment1.setEnabled(false);
            payment2.setEnabled(false);
        } else if (paymentMethod.equals("wallet")) {
            payment1.setChecked(true);
            payment3.setEnabled(false);
            payment2.setEnabled(false);
        } else if (paymentMethod.equals("online")) {
            payment2.setChecked(true);
            payment1.setEnabled(false);
            payment3.setEnabled(false);
        }

        rgOrderStatus.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.rbDeliver:
                        status = "delivered";
                        break;
                    case R.id.rbRefused:
                        status = "refused";
                        break;
                }
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(status==null){
                    Toast.makeText(getContext(), "Select Order Status.", Toast.LENGTH_SHORT).show();
                }else if(image1==null || image2==null){
                    Toast.makeText(getContext(), "Please upload images.", Toast.LENGTH_SHORT).show();
                }else {
                    submitOrder(AppConstants.orderModelSelected.getOrderId().toString(), status, image1, image2,
                            SharedPrefManager.getInstance(getContext()).getUserToken());
                }
            }
        });

        ivCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (PermissionsClass.checkAndRequestPermission(getActivity())) {
                    callCameraApp(CAMERA_PIC_REQUEST_1);
                }
            }
        });

        ivCamera2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (PermissionsClass.checkAndRequestPermission(getActivity())) {
                    callCameraApp(CAMERA_PIC_REQUEST_2);
                }
            }
        });
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
    }

    private void submitOrder(String orderId, String orderStatus, File file1, File file2, String token) {
        if (new ApplicationClass().isInternetAvailable(getContext())) {
            RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file1);
            RequestBody requestFile2 = RequestBody.create(MediaType.parse("image/*"), file2);

            MultipartBody.Part body = MultipartBody.Part.createFormData("image1", file1.getName(), requestFile);
            MultipartBody.Part body2 = MultipartBody.Part.createFormData("image2", file1.getName(), requestFile2);
            RequestBody order_id = RequestBody.create(okhttp3.MultipartBody.FORM, orderId);
            RequestBody status = RequestBody.create(okhttp3.MultipartBody.FORM, orderStatus);

            ApplicationClass.showProgressDialog(getActivity());
            Call<LoginResponse> call = ApiService.createApiService().submitOrder(order_id, status, body, body2, "Bearer " + token);
            call.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    ApplicationClass.hideProgressDialog();
                    try {
                        Log.e("SubmitOrder", response.body().getMessage());
                        if (response.body().getStatus()==1) {
                            ((DashboardActivity) getActivity()).showAlertMsgWithFinish(response.body().getMessage());
                        } else {
                            ApplicationClass.showAlertMsg(getContext(), response.body().getMessage());
                        }
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    ApplicationClass.hideProgressDialog();
                    Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(getContext(), "No internet connection.", Toast.LENGTH_SHORT).show();
        }

    }


    public void callCameraApp(int code) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, code);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PermissionsClass.PERMISSIONS_REQUEST_CODE: {
                Map<String, Integer> perms = new HashMap<String, Integer>();
                // Initial
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for ACCESS_FINE_LOCATION
                if (perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    // All Permissions Granted
                    //callCameraApp();
                } else {
                    // Permission Denied
                    openSettings();
                }
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == CAMERA_PIC_REQUEST_1) {
                Bundle extras = data.getExtras();
                Bitmap bitmap = (Bitmap) extras.get("data");
                image1 = createFile(bitmap);
                ivCamera.setImageBitmap(bitmap);
            } else {
                Bundle extras = data.getExtras();
                Bitmap bitmap = (Bitmap) extras.get("data");
                image2 = createFile(bitmap);
                ivCamera2.setImageBitmap(bitmap);
            }
        }
    }

    private File createFile(Bitmap bitmap) {
        //create a file to write bitmap data
        String filename = System.currentTimeMillis()+".png";
        File file = new File(getActivity().getCacheDir(), filename);
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //Convert bitmap to byte array
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
        byte[] bitmapdata = bos.toByteArray();

        //write the bytes in file
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            fos.write(bitmapdata);
            fos.close();
            fos.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return file;
    }


    private void openSettings() {
        Intent intent = new Intent();
        intent.setAction(
                Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package",
                BuildConfig.APPLICATION_ID, null);
        intent.setData(uri);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

}