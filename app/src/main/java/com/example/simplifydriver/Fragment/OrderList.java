package com.example.simplifydriver.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.simplifydriver.Activity.DashboardActivity;
import com.example.simplifydriver.Adapter.OrderListAdapter;
import com.example.simplifydriver.HelperClasses.AppConstants;
import com.example.simplifydriver.HelperClasses.ApplicationClass;
import com.example.simplifydriver.HelperClasses.SharedPrefManager;
import com.example.simplifydriver.Models.OrderModel;
import com.example.simplifydriver.Networking.ApiService;
import com.example.simplifydriver.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderList extends Fragment implements OrderListAdapter.ItemClickListener {
    RecyclerView rvOrderList;
    ArrayList<OrderModel.OrderList> orderList;
    OrderListAdapter orderListAdapter;
    ImageView ivEmpty;
    TextView tvNoData;
    ProgressBar progressBar;
    SwipeRefreshLayout swipeLayout;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_order_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvOrderList = view.findViewById(R.id.rvOrderList);
        ivEmpty = view.findViewById(R.id.ivEmpty);
        tvNoData = view.findViewById(R.id.tvNoData);
        progressBar = view.findViewById(R.id.progressBar);
        swipeLayout = view.findViewById(R.id.swipeLayout);

        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getOrderList(SharedPrefManager.getInstance(getContext()).getUserToken());
                swipeLayout.setRefreshing(false);
            }
        });

    }

    private void getOrderList(String token) {
        if (new ApplicationClass().isInternetAvailable(getContext())) {
            progressBar.setVisibility(View.VISIBLE);
            Call<OrderModel> call = ApiService.createApiService().getOrderList("Bearer " + token);
            call.enqueue(new Callback<OrderModel>() {
                @Override
                public void onResponse(Call<OrderModel> call, Response<OrderModel> response) {
                    try {
                        progressBar.setVisibility(View.GONE);
                        orderList = new ArrayList<>();
                        orderList = response.body().getOrderList();
                        if (orderList.size() > 0) {
                            orderListAdapter = new OrderListAdapter(orderList, getContext(), OrderList.this);
                            tvNoData.setVisibility(View.GONE);
                            ivEmpty.setVisibility(View.GONE);
                            rvOrderList.setVisibility(View.VISIBLE);
                            rvOrderList.setAdapter(orderListAdapter);
                        } else {
                            tvNoData.setVisibility(View.VISIBLE);
                            ivEmpty.setVisibility(View.VISIBLE);
                            rvOrderList.setVisibility(View.GONE);
                        }

                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<OrderModel> call, Throwable t) {
                    Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);
                }
            });
        } else {
            Toast.makeText(getContext(), "No internet connection.", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onOrderItemClick(int position) {
        AppConstants.orderModelSelected = orderList.get(position);
        ((DashboardActivity) getActivity()).loadFragment(new OrderDetail());
    }

    @Override
    public void onResume() {
        super.onResume();
        getOrderList(SharedPrefManager.getInstance(getContext()).getUserToken());
    }
}