package com.example.simplifydriver.Networking;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.example.simplifydriver.Activity.SplashActivity;
import com.example.simplifydriver.HelperClasses.ApplicationClass;
import com.example.simplifydriver.HelperClasses.SharedPrefManager;
import com.example.simplifydriver.Models.LoginResponse;
import com.example.simplifydriver.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        try {
            Log.e("onMessageRecieved", remoteMessage.getNotification().getTitle());
            Log.e("onMessageRecieved", "Body- "+remoteMessage.getData().toString());
            if(!remoteMessage.getData().isEmpty()){
                if(remoteMessage.getData().containsKey("message")) {
                    sendNotification(remoteMessage.getData().get("message"));
                }
            }
        }catch (NullPointerException e){}
    }

    private void sendNotification(String messageBody) {
        Intent intent = new Intent(this, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder = null;
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    getString(R.string.default_notification_channel_id),
                    getString(R.string.default_notification_channel_name),
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            channel.setDescription(getPackageName());
            notificationManager.createNotificationChannel(channel);
            if (notificationBuilder == null) {
                notificationBuilder = new NotificationCompat.Builder(getApplication(), getPackageName());
            }
        } else {
            if (notificationBuilder == null) {
                notificationBuilder = new NotificationCompat.Builder(getApplication(),getPackageName());
            }
        }
        notificationBuilder.setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        notificationManager.notify(5 /* ID of notification */, notificationBuilder.build());
    }

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        Log.e("onNewToken", s);
        String userId= SharedPrefManager.getInstance(this).getUserId();
        if(userId!=null){
            saveTokenToServer(userId, s);
        }
    }


    private void saveTokenToServer(String userId, String token){
        if (new ApplicationClass().isInternetAvailable(this)) {
            Call<LoginResponse> call = ApiService.createApiService().saveToken(userId, token);
            call.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    try {
                        Log.e("FCM", response.body().getMessage());
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                }
            });
        } else {
            Toast.makeText(this, "No internet connection.", Toast.LENGTH_SHORT).show();
        }
    }
}
