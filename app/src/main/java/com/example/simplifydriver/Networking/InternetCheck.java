package com.example.simplifydriver.Networking;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkRequest;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.simplifydriver.HelperClasses.ApplicationClass;

public class InternetCheck {

    private ConnectivityManager connectivityManager = null;
    private ConnectivityManager.NetworkCallback networkCallback;

    public void internetCallback(Boolean connectivity, final Context context) {
        if(connectivity){
            connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if(!new ApplicationClass().isInternetAvailable(context)){
                Toast.makeText(context, "No internet access.", Toast.LENGTH_SHORT).show();
            }
            NetworkRequest networkRequest = new NetworkRequest.Builder().build();
            networkCallback = new ConnectivityManager.NetworkCallback(){
                @Override
                public void onAvailable(@NonNull Network network) {
                    super.onAvailable(network);
                    Toast.makeText(context, "Internet connected.", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onLost(@NonNull Network network) {
                    super.onLost(network);
                    Toast.makeText(context, "You lost internet access.", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onUnavailable() {
                    super.onUnavailable();
                    Toast.makeText(context, "No internet access.", Toast.LENGTH_SHORT).show();
                }
            };
            try {
                connectivityManager.registerNetworkCallback(networkRequest, networkCallback);
            }catch (Exception e){
                e.printStackTrace();
            }
        }else {
            try {
                connectivityManager.unregisterNetworkCallback(networkCallback);
            }catch (Exception e){
                e.printStackTrace();
            }

        }
    }
}
