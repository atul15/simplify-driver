package com.example.simplifydriver.Networking;


import com.example.simplifydriver.Models.LoginResponse;
import com.example.simplifydriver.Models.NotificationModel;
import com.example.simplifydriver.Models.OrderModel;
import com.example.simplifydriver.Models.SettingsResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ApiClient {

    @FormUrlEncoded
    @POST("driver-login")
    Call<LoginResponse> loginAction(
            @Field("username") String username,
            @Field("password") String password,
            @Field("device_token") String device_token
    );


    @Headers("Content-Type: application/json")
    @GET("driver-order-list")
    Call<OrderModel> getOrderList(
            @Header("Authorization") String authHeader);

    @FormUrlEncoded
    @POST("start-order-trip")
    Call<LoginResponse> startTrip(
            @Field("order_id") String order_id,
            @Header("Authorization") String token
    );

    @Multipart
    @POST("live-location")
    Call<LoginResponse> saveLiveLocation(
            @Part("user_id") RequestBody user_id,
            @Part("order_id") RequestBody order_id,
            @Part("lat") RequestBody lat,
            @Part("lng") RequestBody lng,
            @Part("current_location") RequestBody current_location,
            @Part("state") RequestBody state,
            @Part("total_run") RequestBody total_run,
            @Part("pause_time") RequestBody pause_time,
            @Part("total_distance") RequestBody total_distance,
            @Header("Authorization") String authHeader);

//
//    @FormUrlEncoded
//    @POST("login-action")
//    Call<LoginResponse> forgotPass(
//            @Field("emailMobile") String emailMobile
//    );
//
    @FormUrlEncoded
    @POST("order-loading-status")
    Call<LoginResponse> orderLoadingStatus(
            @Field("order_id") String order_id,
            @Field("loading_start_time") String loading_start_time,
            @Field("loading_end_time") String loading_end_time,
            @Header("Authorization") String token
    );

    @Multipart
    @POST("order-completed-status")
    Call<LoginResponse> submitOrder(
            @Part("order_id") RequestBody order_id,
            @Part("status") RequestBody verified_load_id,
            @Part MultipartBody.Part image1,
            @Part MultipartBody.Part image2,
            @Header("Authorization") String authHeader);


    @FormUrlEncoded
    @POST("save-device-token")
    Call<LoginResponse> saveToken(
            @Field("user_id") String user_id,
            @Field("device_token") String device_token
    );


    @FormUrlEncoded
    @POST("get-notification")
    Call<NotificationModel> getNotificationList(
            @Field("user_id") String user_id,
            @Field("read_at") String read_at,
            @Header("Authorization") String authHeader);

    @Headers("Content-Type: application/json")
    @GET("all-settings")
    Call<SettingsResponse> getAllSettings(
            @Header("Authorization") String authHeader);

    @FormUrlEncoded
    @POST("delete-notifications")
    Call<LoginResponse> deleteNotifications(
            @Field("user_id") String user_id,
            @Field("notification_ids") String notification_ids,
            @Header("Authorization") String token
    );

}
